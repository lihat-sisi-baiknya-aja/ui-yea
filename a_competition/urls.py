from django.urls import path
from . import views

urlpatterns = [
    path('Paper_Competition/', views.paperCompetition, name='paperCompetition'),
    path('Enviroaction/', views.enviroaction, name='enviroaction'),
    path('Enviroaction/Carousel', views.get_carousel, name='carousel'),
]