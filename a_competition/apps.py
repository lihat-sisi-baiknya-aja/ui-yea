from django.apps import AppConfig


class ACompetitionConfig(AppConfig):
    name = 'a_competition'
