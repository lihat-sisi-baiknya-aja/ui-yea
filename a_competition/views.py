from django.shortcuts import render

response = {}


def paperCompetition(request):
    response = {
        'title': 'Paper Competition',
        'header_title': 'International Paper\nCompetition',
        'image0': '/static/img/Landing/Paper_Com.png',
        'image1': '/static/img/paper1.jpg',
        'image2': '/static/img/paper2.jpg',
        'image3': '/static/img/paper3.jpg',
        'image4': '/static/img/paper4.jpg',
        'image5': '/static/img/paper5.jpg',
        'prize1': 'Best Paper\n(for each subtheme)​ :',
        'prize2': 'Best Poster\n(for each subtheme)​ :',
        'prize3': 'Best Delegate :',
        'kategori_1' : '',
        'kategori_2' : '',
        'kategori_3' : '',
        'date': 'June 8 - July 7',
        'description': 'The world is seeking new innovations for environmental issues and here is where you can contribute yours. Send your best abstract about the environmental issues that you are interested in, and your chance to make a real impact for mother earth is ahead!',
        'guidebook_url': 'http://bit.ly/UIYEApapergdrive',
        'register_url': 'http://bit.ly/UIYEA19PaperCompetition',
        'cp_number': ['Aisyah +62 85695501528' , 'Kaifa +62 87822924403' ],
        'carousel': '1'
    }
    return render(request, 'competition_templates.html', response)


def enviroaction(request):
    response = {
        'title': 'EnviroAction',
        'header_title': 'EnviroAction',
        'image0': '/static/img/Landing/EnviroAct.png',
        'prize1': '1st Place​ :',
        'prize2': '2nd Place :',
        'prize3': '3rd Place :',
        'date': 'August 26 - October 23',
        'description': 'Environmental Action is a social project competition in the form of a limited duration video with the theme of “Green Urban and Community” and must be posted by the participant’s social media. Participants are also required to make a caption to describe the social movement that has been done. The judges of this competition are an influencer that is concern about community engagement and cares about the environment. This social project competition is open general without age restrictions.',
        'item_list': ['Who may enter: Open for public', 'Submission fee: FREE'],
        'guidebook_url': 'To Be Announced',
        'register_url': 'http://bit.ly/EnviroAction2019',
        'kategori_1' : 'To Be Announced',
        'kategori_2' : 'To Be Announced',
        'kategori_3' : 'To Be Announced',
        'cp_number': ['Ophie +62 87772274464 | LINE: phiemin' , 'Hana +62 8158151426 | LINE: aliafarhana1502' ],
        'carousel': '0'
    }
    return render(request, 'competition_templates.html', response)


def get_carousel(request):
    response = {
        'image1': '/static/img/paper1.jpg',
        'image2': '/static/img/paper2.jpg',
        'image3': '/static/img/paper3.jpg',
        'image4': '/static/img/paper4.jpg',
        'image5': '/static/img/paper5.jpg',
    }
    return render(request, 'carousel.html', response)
