from django.shortcuts import render

response = {}


def about(request):
    return render(request, 'about.html', response)
