# UI YEA 2019 Website Project 
-------------------
UI YEA 2019 Website Project is a formal project requested by UI YEA Organization to the no-named team (represented by Bagus Pribadi) for UI YEA 2019. This website is using Django Framework.

-------------
### Project Member:
  - Bagus Pribadi
  - Steffi Alexandra
  - Nathanael
----------
Documents:
  - 
  - 
---------------------
### Requirements:
- git command-line
- Python (with pip installed)
---------------
#### Local Testing
Clone this repo:
```sh
$ git clone https://gitlab.com/lihat-sisi-baiknya-aja/UIYEA 
```
Use virtual environment:
```sh
$ python -m venv env
```
Install the Requirements:
```sh
$ pip install -r requirements.txt
```
Make migrations and Migrate:
```sh
$ python manage.py makemigrations
$ python manage.py migrate
```
Run local server:
```sh
$ python manage.py runserver
```
Open your local server on browser, by default:
```sh
$ localhost:8000
```

License
----

MIT


**The code are free to use as reference, because I took it from the other reference too ~~**