from django.shortcuts import render

def envirofest(request):
    response = {
        'title': 'Green Festival',
        'header_title': 'Green Festival',
        'image0' : '/static/img/Landing/EnviroFest.png',
        'image1': '/static/img/collage1.jpg',
        'image2': '/static/img/collage2.jpg',
        'image3': '/static/img/collage3.jpg',
        'register_url' : '',
        'description': 'The purpose of Green Festival is to make the society feels the euphoria of UI YEA 2019. Wrapped in a form of festival held in a public place, we aim to raise public’s awareness about environmental issues in interesting ways. Environmental campaign, attractive entertainers, and many more surprises will definitely make your weekend worthwhile!',
        'item_list': ['Place: Car Free Day Jakarta (Sudirman Street)', 'Who may enter: Everyone', 'Entry Fee: FREE']
    }
    return render(request, 'preEvent_template.html', response)


def roadshow(request):
    response = {
        'title': 'Campus Roadshow',
        'header_title': 'Campus Roadshow',
        'image0': '/static/img/Landing/Roadshow.png',
        'image1': '/static/img/collage1.jpg',
        'image2': '/static/img/collage2.jpg',
        'image3': '/static/img/collage3.jpg',
        'description': 'Campus Roadshow is one of the series of events where we invite young people directly to raise awareness and care more about the environment with a variety of interesting activities. The roadshow will be held for all faculties at Universitas Indonesia, several universities in Indonesia, and will reach Southeast Asia too! The roadshow also aims to invite all of you to take part in the competition and attend the peak of the UI Yea 2019 event in the International Summit.',
    }
    return render(request, 'preEvent_template.html', response)

def carousel5(request):
    response = {
        'image1': '/static/img/preevent/roadshow/1.jpg',
        'image2': '/static/img/preevent/roadshow/2.jpg',
        'image3': '/static/img/preevent/roadshow/3.jpg',
        'image4': '/static/img/preevent/roadshow/4.jpg',
        'image5': '/static/img/preevent/roadshow/5.jpg',
    }
    return render(request, 'carousel.html', response)

def carousel6(request):
    response = {
        'image1': '/static/img/dokum/IMG_0994.JPG',
        'image2': '/static/img/dokum/IMG_1042.JPG',
        'image3': '/static/img/dokum/IMG_1081.JPG',
        'image4': '/static/img/dokum/IMG_1145.JPG',
        'image5': '/static/img/dokum/IMG_1301.JPG',
    }
    return render(request, 'carousel.html', response)