from django.urls import path
from . import views

urlpatterns = [
    path('green-festival/', views.envirofest, name='envirofest'),
    path('roadshow/', views.roadshow, name='roadshow'),
    path('roadshow/Carousel', views.carousel5, name='carousel5'),
    path('roadshow/Carousel6', views.carousel6, name='carousel6'),

]