from django.shortcuts import render

response = {}


def home(request):
    return render(request, 'home.html', response)
