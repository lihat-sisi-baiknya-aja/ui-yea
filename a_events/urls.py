from django.urls import path
from . import views

urlpatterns = [
    path('PPandFG/', views.PPandFG, name='PPandFG'),
    path('PPandFG/Carousel', views.get_carousel1, name='carousel1'),
    path('fieldTrip/', views.fieldTrip, name='fieldTrip'),
    path('fieldtrip/Carousel', views.get_carousel2, name='carousel2'),
    path('culturalNight/', views.culturalNight, name='culturalNight'),
    path('culturalNight/Carousel', views.get_carousel3, name='carousel3'),
    path('internationalSummit/', views.internationalSummit, name='internationalSummit'),
    path('internationalSummit/Carouse4', views.get_carousel4, name='carousel4'),
]