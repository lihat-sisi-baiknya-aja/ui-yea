from django.shortcuts import render

def PPandFG(request):
    response = {
        'title': 'Paper Presentation and Focus Group Discussion',
        'image0' : '/static/img/Landing/PP&FG.png',
        'header_title': 'Paper Presentation\nand\nFocus Group Discussion',
        'bg_image': "url('/static/img/firstday.jpg')",
        'date': "14 November 2019",
        'location': "Aula Terapung, Universitas Indonesia",
        'register_url' : '',
        'image1': '/static/img/firstday1.jpg',
        'image2': '/static/img/firstday2.jpg',
        'image3': '/static/img/firstday3.jpg',
        'description': 'Here is where the best ideas is presented in public. Judged by the expertises from various environmental institutions, you sure would not want to miss such insight you will get. After all the chosen papers have been presented, there will be focus group discussion where the delegates can conclude a real solution for environmental problem that is currently happening. Final solution from FGD will be sent to the related caretakers so they can implement your solutions.',
        'item_list': ['Place: Aula Terapung, University of Indonesia','Who may enter: Open for public as the audience']
    }
    return render(request, 'main_event.html', response)

def fieldTrip(request):
    response = {
        'title': 'Field Trip',
        'image0' : '/static/img/Landing/Field_Trip.png',
        'header_title': 'Field Trip',
        'bg_image': "url('/static/img/secondday.jpg')",
        'date': "15 November 2019",
        'location': "To Be Announced",
        'image1': '/static/img/secondday1.jpg',
        'image2': '/static/img/secondday2.jpg',
        'image3': '/static/img/secondday3.jpg',
        'description': 'Field trip is designed to be the event where the delegations can expand their knowledge about environmental issues from the partner company. After that, they will have a chance to implement their knowledge by doing real actions that directly have an impact to the environment.',
        'item_list': ['Place:','Who may enter: All delegates only']
    }
    return render(request, 'main_event.html', response)

def culturalNight(request):
    response = {
        'title': 'Cultural Night',
        'image0' : '/static/img/Landing/Cultural_night.png',
        'header_title': 'Cultural Night',
        'bg_image': "url('/static/img/thirdday.jpg')",
        'date': "15 November 2019",
        'location': "Savero Hotel, Depok",
        'image1': '/static/img/thirdday1.jpg',
        'image2': '/static/img/thirdday2.jpg',
        'image3': '/static/img/thirdday3.jpg',
        'description': 'Cultural night is the closing event after the delegates conducted a field trip. this is a gala dinner for delegates and a media for them to get to know each other personally. Some delegates will also convey the issues they brought to the UI YEA 2019 and play an exciting games to give pleasure for all delegates.',
        'item_list': ['Place: Aula Terapung, University of Indonesia','Place: Aula Terapung, University of Indonesia']
    }
    return render(request, 'main_event.html', response)

def internationalSummit(request):
    response = {
        'title': 'International Summit',
        'image0' : '/static/img/Landing/International_Summit.png',
        'header_title': 'International Summit',
        'bg_image': "url('/static/img/fourthday.jpg')",
        'date': "16 November 2019",
        'location': "Pesona Square Ballroom, Depok",
        'register_url' : '',
        'description': 'The International Summit is the highlight of the UI YEA 2019 event which presents various kinds of environmental practitioners where many speakers and influencers come to Inspire others! On top of that, there will be a music performance and community corner as a form of cooperation from various environmental communities. Do not miss the announcement of the winner because we will announce it here!',
        'item_list': ['Place: Balairung Universitas Indonesia','Who may enter: Open for Public','Entry Fee: FREE']
    }
    return render(request, 'main_event.html', response)

def get_carousel1(request):
    response = {
        'image1': '/static/img/event/paper/1.JPG',
        'image2': '/static/img/event/paper/2.JPG',
        'image3': '/static/img/event/paper/3.JPG',
        'image4': '/static/img/event/paper/4.JPG',
        'image5': '/static/img/event/paper/5.JPG',
    }
    return render(request, 'carousel.html', response)

def get_carousel2(request):
    response = {
        'image1': '/static/img/event/field/1.JPG',
        'image2': '/static/img/event/field/2.JPG',
        'image3': '/static/img/event/field/3.JPG',
        'image4': '/static/img/event/field/4.JPG',
        'image5': '/static/img/event/field/5.JPG',
    }
    return render(request, 'carousel.html', response)

def get_carousel3(request):
    response = {
        'image1': '/static/img/event/cultural/1.JPG',
        'image2': '/static/img/event/cultural/2.JPG',
        'image3': '/static/img/event/cultural/3.JPG',
        'image4': '/static/img/event/cultural/4.JPG',
        'image5': '/static/img/event/cultural/5.JPG',
    }
    return render(request, 'carousel.html', response)

def get_carousel4(request):
    response = {
        'image1': '/static/img/event/international/1.JPG',
        'image2': '/static/img/event/international/2.JPG',
        'image3': '/static/img/event/international/3.JPG',
        'image4': '/static/img/event/international/4.JPG',
        'image5': '/static/img/event/international/5.JPG',
    }
    return render(request, 'carousel.html', response)

