from django.apps import AppConfig


class AEventsConfig(AppConfig):
    name = 'a_events'
